hello: hello.lua 
	luac -o hello.o hello.lua

clean:
	rm -f ./hello.o

run:
	lua hello.o

all: hello run
	
